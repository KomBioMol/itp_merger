from itertools import product
import sys
from optparse import OptionParser


class Itp:
    def __init__(self, filename):
        self.content = [line for line in open(filename)]
        self.sections_content, self.section_names = self.get_sections()
        self.bonds = None
        self.get_bonds()
        self.natoms = self.count_atoms()
    
    def get_sections(self):
        section_list = [line.strip('[ ]\n') for line in self.content if line.strip().startswith('[')]
        section_content = []
        counter = -1
        for line in self.content:
            if line.strip().startswith('['):
                section_content.append([])
                counter += 1
            if counter >= 0:
                section_content[counter].append(line)
        return section_content, section_list
    
    def count_atoms(self):
        lines = self.get_section_content('atoms')
        non_empty = [line for line in lines if line.strip() and not line.startswith(';')]
        return int(non_empty[-1].split()[0])
    
    def get_section_content(self, query_name, index=0):
        order = [num for num, sec_name in enumerate(self.section_names) if sec_name == query_name][index]
        return self.sections_content[order]
    
    def set_section_content(self, query_name, new_content, index=0):
        order = [num for num, sec_name in enumerate(self.section_names) if sec_name == query_name][index]
        self.sections_content[order] = new_content
    
    def add_section_content(self, section_name, new_content, index=0):
        order = [num for num, sec_name in enumerate(self.section_names) if sec_name == section_name][index]
        self.sections_content[order].append(new_content)
    
    def get_bonds(self):
        lines = self.get_section_content('bonds')
        bond_list = []
        for line in lines:
            if line.split() and line.split()[2] == "1":
                bond_list.append((int(line.split()[0]), int(line.split()[1])))
        self.bonds = bond_list
    
    def make_bond(self, atom_own, atom_other, other):
        other.get_bonds()
        new_bond = [tuple(sorted([int(atom_own), int(atom_other)]))]
        new_angles = self.generate_angles(other, atom_own, atom_other)
        new_pairs, new_dihedrals = self.generate_14(other, atom_own, atom_other)
        self.add_entries(new_bond, 'bonds', '1')
        self.add_entries(new_pairs, 'pairs', '1')
        self.add_entries(new_angles, 'angles', self.check_parm_type('angles', 3))
        self.add_entries(new_dihedrals, 'dihedrals', self.check_parm_type('dihedrals', 4))
        
    def check_parm_type(self, parmtype, npar):
        section_content = self.get_section_content(parmtype, 0)
        for line in section_content:
            lspl = line.split()
            if len(lspl) > npar and not line.strip().startswith(';') and not line.strip().startswith('['):
                return lspl[npar]
        
    def generate_angles(self, other, atom_own, atom_other):
        neigh_atoms_1 = [[b for b in bond if b != atom_own][0] for bond in self.bonds if atom_own in bond]
        neigh_atoms_2 = [[b for b in bond if b != atom_other][0] for bond in other.bonds if atom_other in bond]
        new_angles = [(at1, atom_own, atom_other) for at1 in neigh_atoms_1]
        new_angles += [(atom_own, atom_other, at2) for at2 in neigh_atoms_2]
        return new_angles
    
    def generate_14(self, other, atom_own, atom_other):
        # atoms directly neighboring with the new bond
        neigh_atoms_1 = [[b for b in bond if b != atom_own][0] for bond in self.bonds if atom_own in bond]
        neigh_atoms_2 = [[b for b in bond if b != atom_other][0] for bond in other.bonds if atom_other in bond]
        # atoms only neighboring with atoms from the above lists
        neigh_atoms_11 = [list(set(bond).difference(set(neigh_atoms_1)))[0] for bond in self.bonds
                          if set(neigh_atoms_1) & set(bond) and atom_own not in bond]
        neigh_atoms_21 = [list(set(bond).difference(set(neigh_atoms_2)))[0] for bond in other.bonds
                          if set(neigh_atoms_2) & set(bond) and atom_other not in bond]
        new_pairs = list(product(neigh_atoms_1, neigh_atoms_2)) + list(product([atom_own], neigh_atoms_21)) + \
            list(product([atom_other], neigh_atoms_11))
        new_dihedrals = [(a, atom_own, atom_other, d) for a, d in list(product(neigh_atoms_1, neigh_atoms_2))]
        new_dihedrals += [(a, b, atom_own, atom_other) for a in neigh_atoms_11 for b in neigh_atoms_1
                          if (a, b) in self.bonds or (b, a) in self.bonds]
        new_dihedrals += [(atom_own, atom_other, c, d) for d in neigh_atoms_21 for c in neigh_atoms_2
                          if (c, d) in self.bonds or (d, c) in self.bonds]
        return new_pairs, new_dihedrals
    
    def add_entries(self, entries, section_name, interaction_type):
        interaction_type = str(interaction_type)
        for entry in entries:
            entry = [str(i) for i in sorted([int(x) for x in entry])]
            formatted_entry = ((len(entry)+1) * '{:>5s} ').format(*entry, interaction_type)
            self.add_section_content(section_name, formatted_entry)
    
    def offset_numbering(self, offset, startfrom=0):
        offset = int(offset)
        self.offset_atoms(offset, startfrom)
        self.offset_params(offset, startfrom)
        
    def offset_atoms(self, offset, startfrom):
        section_content = self.get_section_content('atoms')
        for linenum, line in enumerate(section_content):
            lspl = line.split()
            if len(lspl) > 7 and not lspl[0].startswith(';') and int(lspl[0]) >= startfrom:
                new_line = ('{:6d}{:>11s}{:>7s}{:>7s}{:>7s}{:>7d} ' +
                            (len(lspl)-6) * '{:>10s} ').format(int(lspl[0])+offset, *lspl[1:5],
                                                               int(lspl[5])+offset, *lspl[6:])
                section_content[linenum] = new_line
        self.set_section_content('atoms', section_content)
        
    def offset_params(self, offset, startfrom):
        for section in [('bonds', 0, 2), ('angles', 0, 3), ('pairs', 0, 2), ('dihedrals', 0, 4), ('dihedrals', 1, 4)]:
            section_content = self.get_section_content(section[0], section[1])
            npar = section[2]
            for linenum, line in enumerate(section_content):
                lspl = line.split()
                if len(lspl) > npar and not lspl[0].startswith('[') and not lspl[0].startswith(';'):
                    new_line = (npar * '{:>5d} ' + '{:>5s} ' +
                                (len(lspl)-npar-1)*'{:>10s} ').format(*[int(x) + (offset * (int(x) >= startfrom))
                                                                        for x in lspl[:npar]], *lspl[npar:])
                    section_content[linenum] = new_line
            self.set_section_content(section[0], section_content, section[1])
    
    def merge_two(self, other, anchor_own, anchor_other):
        anchor_other = int(anchor_other)
        anchor_own = int(anchor_own)
        if other is not self:
            other.offset_numbering(self.natoms)
            anchor_other += self.natoms
        self.make_bond(anchor_own, anchor_other, other)
        self.merge_fields(other)
    
    def merge_fields(self, other):
        for section in [('atoms', 0), ('bonds', 0), ('angles', 0), ('pairs', 0), ('dihedrals', 0), ('dihedrals', 1)]:
            content = other.get_section_content(section[0], section[1])
            for line in content:
                if line.strip() and not line.strip().startswith(';') and not line.strip().startswith('['):
                    self.add_section_content(section[0], line, section[1])
                    
    def sort_bottom(self):
        section_content = self.get_section_content('dihedrals', 1)
        first = []
        last = []
        for line in section_content:
            if line.startswith('[') and 'dihedral' in line:
                first.append(line)
            elif len(line.split()) > 4 and line.split()[4] in ['2', '4']:
                try:
                    _ = [int(line.split()[i]) for i in range(4)]
                    first.append(line)
                except ValueError:
                    last.append(line)
            elif line.startswith(';') and 'ai' in line and 'aj' in line:
                first.append(line)
            else:
                last.append(line)
        self.set_section_content('dihedrals', first + ['\n'] + last, 1)
        
    def delete_entries(self, atom):
        self.delete_atom(atom)
        self.delete_params(atom)
        
    def delete_atom(self, atom):
        section_content = self.get_section_content('atoms')
        new_content = []
        for line in section_content:
            lspl = line.split()
            if not (len(lspl) > 7 and lspl[0][0] not in (';', '[') and int(lspl[0]) == int(atom)):
                new_content.append(line)
        self.set_section_content('atoms', new_content)
    
    def delete_params(self, atom):
        for section in [('bonds', 0, 2), ('angles', 0, 3), ('pairs', 0, 2), ('dihedrals', 0, 4), ('dihedrals', 1, 4)]:
            section_content = self.get_section_content(section[0], section[1])
            new_content = []
            npar = section[2]
            for line in section_content:
                lspl = line.split()
                if len(lspl) > npar and not lspl[0].startswith('[') and not lspl[0].startswith(';'):
                    if not any([int(x) == int(atom) for x in lspl[:npar]]):
                        new_content.append(line)
                else:
                    new_content.append(line)
            self.set_section_content(section[0], new_content, section[1])
            
    def delete(self, string, options, num):
        nums_to_delete = sorted([int(x) for x in string.strip().split()], key=lambda x: -x)
        for atom in nums_to_delete:
            self.delete_entries(atom)
            if options.itp2 == "no":
                if options.n1 > atom:
                    options.n1 -= 1
                if options.n2 > atom:
                    options.n2 -= 1
            else:
                if num == 1:
                    if options.n1 > atom:
                        options.n1 -= 1
                elif num == 2:
                    if options.n2 > atom:
                        options.n2 -= 1
        for atom in nums_to_delete:
            self.offset_numbering(-1, atom)
            self.natoms -= 1
    
    def write(self, outputfile):
        self.sort_bottom()
        with open(outputfile, 'w') as o:
            for section in self.section_names:
                for num in range(len([x for x in range(len(self.section_names)) if section == self.section_names[x]])):
                    for line in self.get_section_content(section, num):
                        if line.strip():
                            if line.strip().startswith('['):
                                o.write('\n')
                            if not line.endswith('\n'):
                                o.write(line + '\n')
                            else:
                                o.write(line)


def parse_all():
    parser = OptionParser(usage="")
    parser.add_option("-a", dest="itp1", action="store", type="string",
                      help="First .itp file (can be the only if the new bond is intramolecular)")
    parser.add_option("-b", dest="itp2", action="store", type="string", default="no",
                      help="Second .itp file")
    parser.add_option("-i", dest="n1", action="store", type="int",
                      help="First atom number (corresponding to the -a file numbering) "
                           "that will be involved in the new bond; 1-based indexing")
    parser.add_option("-j", dest="n2", action="store", type="int",
                      help="Second atom number (corresponding to the -b file numbering if -b is passed, "
                           "or -a otherwise) that will be involved in the new bond; 1-based indexing")
    parser.add_option("-p", dest="del1", action="store", type="string", default="no",
                      help="Atoms (e.g. excess hydrogens) to be removed from the -a file")
    parser.add_option("-q", dest="del2", action="store", type="string", default="no",
                      help="Atoms (e.g. excess hydrogens) to be removed from the -b file")
    (options, args) = parser.parse_args()
    return options


if __name__ == "__main__":
    opts = parse_all()
    itp1 = Itp(opts.itp1)
    if opts.del1 != "no":
        itp1.delete(opts.del1, opts, 1)
    if opts.itp2 != "no":
        itp2 = Itp(opts.itp2)
        if opts.del2 != "no":
            itp2.delete(opts.del2, opts, 2)
        itp1.merge_two(itp2, opts.n1, opts.n2)
    elif opts.itp2 == "no":
        itp1.merge_two(itp1, opts.n1, opts.n2)
    itp1.write('final.itp')
